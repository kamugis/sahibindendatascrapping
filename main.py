import SearchResult as SR
import ResultScrapper as RS



f = open("out/Result.csv", "w")

RS.csv_header(f)


df = SR.GetResults(offset_limit=50, export_path="out/pages.csv")


for link in RS.links(df):
    t = RS.getHTML(link)
    parsed = RS.parseResponse(t, link)
    RS.export(f, parsed)
