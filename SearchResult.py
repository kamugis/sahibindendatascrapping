import pandas
import requests
from bs4 import BeautifulSoup as bs

# Main results dictionary. This is the state 
# object of this module.
__RESULTS = {"id": [], "loc": [], 
             "price": [], "link": []}


def GetResults(offset=50, page_size=50, offset_limit= 300000, export_path=None):
    for i in getHTML(offset_range=offset, page_size=page_size, offset_limit=offset_limit):
        t = parseHTML(i)
        [addToDict(j) for j in parseResults(t)]

    df = generateDataFrame()

    if export_path:
        df.to_csv(export_path)

    return df

def getHTML(offset_range=50, page_size=50, offset_limit=300):
    # Sahibinden web sitesinin engeline takılmamak için browsermiş gibi davranıyor,
    # İsteklerimize header ekliyoruz.
    header = {'User-Agent': 'Safari/537.36'}

    for i in range(0, offset_limit, page_size):
        uri = generateUrl(i, page_size)
        response = requests.get(uri, headers=header)
        
        yield response.text

def generateUrl(offset, page_size):
    return f"https://www.sahibinden.com/emlak/istanbul?pagingSize={page_size}&pagingOffset={offset}"


def generateDataFrame():
    data = {"loc" : __RESULTS["loc"],
            "price": __RESULTS["price"],
            "link" : __RESULTS["link"]}
    return pandas.DataFrame(data=data, index=__RESULTS["id"])

def addToDict(values):
    __RESULTS["id"].append(values[0])
    __RESULTS["loc"].append(values[1])
    __RESULTS["price"].append(values[2])
    __RESULTS["link"].append("https://www.sahibinden.com" + values[3])

def parseHTML(text):
    soup = bs(text)
    return soup.findAll("tr", class_="searchResultsItem")

def parseResults(results):
    for result in results:
        result_id = result.get("data-id")

        if not result_id:
            continue

        link = result.find("a", class_="classifiedTitle")["href"].strip()
        price = result.find("td", class_="searchResultsPriceValue").text.strip()
        location = result.find("td", class_="searchResultsLocationValue").text.strip()

        yield [result_id, location, price, link]
        print(result_id)

def exportDataFrame(df):
    df.to_csv("out/results.csv")


if __name__ == "__main__":
    import time

    start = time.time()

    df = GetResults(offset_limit=400, export_path="out/results.csv")

    end = time.time()

    print(df)
    print(end-start)