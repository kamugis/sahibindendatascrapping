import SearchResult as SR
import pandas 
import time
import json
import requests
from bs4 import BeautifulSoup as bs
from selenium import webdriver
from selenium.webdriver.chrome.options import Options


header = {'User-Agent': 'Safari/537.36'}
cookies = {
"st":"a89e846803b804fa3b73f1249468b287a51aa47c5369ff58a79907dc729336d52b6714bf430b95b29ad1420deb095e228e377e207120fd924",
"vid":"377",
"cdid":"NdhgkLEykcSWYW8d5e92de3d",
"s3IssGuY1":"A2VQ321xAQAA9-Z8LIO6gbXwulqDjDYKulgrtlCqFBL3mgAFsQR97kbgjqTKAbAo8TOuchShwH8AAEB3AAAAAA==",
"estate360ClassifiedDetailSplashClosed": "true",
"geoipCity":"bursa",
"geoipIsp":"turkcell_superonline",
"__gfp_64b":"ckZ3iudiylBEMmgvSqcOoYk2dQkn4pnQLCIupPTrDZv.m7",
"_fbp":"fb.1.1586686278930.623856260",
"_ga":"GA1.2.781298487.1586686279",
"_gid":"GA1.2.152990080.1586686279",
"__gads":"ID=91a034f39c3eff4e:T=1586686287:S=ALNI_MbQ2fEt0dHiJXvoz2fHlUoqFm5Saw",
"segIds":"254647|211970",
"showCookiePolicy":"true"
}




def links(dataFrame):
    for link in dataFrame["link"]:
        yield link

def getHTML(link):
    r = requests.get(link, headers=header, cookies=cookies)
    time.sleep(1)
    print(link)
    return r.text

def parseResponse(response, link):
    soup = bs(response)

    data = soup.find("div", {"id": "gaPageViewTrackingJson"})
    data = parseTrackingJson(data["data-json"])

    data2 = soup.find("div", {"id": "gmap"})
    latitude = data2["data-lat"]
    longtitude = data2["data-lon"]

    return {
        "id" : data.get("İlan No"),
        "zaman" : data.get("İlan Tarihi"),
        "fiyat" : data.get("ilan_fiyat"),
        "ulke" : data.get("loc1"),
        "il" : data.get("loc2"), 
        "ilce" : data.get("loc3"),
        "semt" : data.get("loc4"),
        "mahalle": data.get("loc5"),
        "kat" : data.get("Bulunduğu Kat"),
        "toplam_kat" : data.get("Kat Sayısı"),
        "yas" : data.get("Bina Yaşı"),
        "alan" : data.get("m² (Net)"),
        "latitude": latitude,
        "longtitude": longtitude,
        "link" : link
    }


def parseTrackingJson(jsonText):
    js = json.loads(jsonText)

    vars = js["customVars"]
    keys = ["Kat Sayısı", "İlan Tarihi", "ilan_fiyat", "loc1", "loc2", "loc3", "loc4", "loc5", "İlan No", "Bulunduğu Kat", "m² (Net)", "Bina Yaşı"]

    parsed = {}

    for i in vars:
        if i["name"] in keys:
            parsed[i["name"]] = i["value"]

    
    return parsed

def csv_header(f):
    f.write("ID;Zaman;Fiyat;Ulke;Il;Ilce;Semt;Mahalle;Kat;Toplam_Kat;Yas;Alan;Enlem;Boylam;Link;")

def export(f, p):
    s = f'\n{p["id"]};{p["zaman"]};{p["fiyat"]};{p["ulke"]};{p["il"]};{p["ilce"]};{p["semt"]};{p["mahalle"]};{p["kat"]};{p["toplam_kat"]};{p["yas"]};{p["alan"]};{p["latitude"]};{p["longtitude"]};{p["link"]};'
    f.write(s)
    f.flush()

if __name__ == "__main__":
    df = SR.GetResults(offset_limit=50)
    print("Done")

    time.sleep(2)
    
    for link in links(df):
        t = getHTML(link)
        print(parseResponse(t, link))





